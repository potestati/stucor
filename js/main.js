$(document).ready(function () {

    $("#owl-demo").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        mouseDrag: true,
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3],
        navigation: true,
        navigationText: ["<", ">"],
        rewindNav: true,
        scrollPerPage: false,
    });

    $("#owl-demo-one").owlCarousel({
        navigation: true, // Show next and prev buttons
        navigationText: [".", "."],
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true


    });
});


